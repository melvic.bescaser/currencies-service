import { inject, injectable } from 'inversify';

import { IDENTIFIERS } from '@configurations/constants';
import { OperationalError } from '@errors/OperationalError';
import { ICacheUtility } from '@interfaces/ICacheUtility';
import { IDatabaseUtility } from '@interfaces/IDatabaseUtility';
import { ICurrenciesRepository } from '@interfaces/ICurrenciesRepository';
import { ICurrenciesService } from '@interfaces/ICurrenciesService';
import { ICurrency } from '@models/ICurrencies';

@injectable()
export class CurrenciesService implements ICurrenciesService {
  constructor(
    @inject(IDENTIFIERS.CURRENCIES_REPOSITORY)
    public currenciesRepository: ICurrenciesRepository,
    @inject(IDENTIFIERS.CACHE_UTILITY) public cacheUtility: ICacheUtility,
    @inject(IDENTIFIERS.DATABASE_UTILITY)
    public databaseUtility: IDatabaseUtility,
  ) {}

  async getCurrencies(): Promise<ICurrency[]> {
    const exists = await this.cacheUtility.exists('currencies-get-currencies');

    if (exists) {
      const currencies = (await this.cacheUtility.get('currencies-get-currencies')) as string;

      return JSON.parse(currencies);
    }

    const currencies = await this.currenciesRepository.getAll();

    this.cacheUtility.set('currencies-get-currencies', JSON.stringify(currencies), { EX: 20 });

    return currencies;
  }

  async getCurrency(code: string): Promise<ICurrency> {
    return await this.databaseUtility.runTransaction(async () => {
      const currency = await this.currenciesRepository.getAllByCode(code);

      if (!currency) {
        throw new OperationalError(`No currency found for '${code}' code.`);
      }

      return currency;
    });
  }
}
