import { mockDatabaseUtility, mockCacheUtility } from '@mocks/utilities';
import { mockCurrenciesRepository } from '@mocks/repositories';

import { CurrenciesService } from './CurrenciesService';

beforeEach(() => {
  jest.clearAllMocks();
});

it('should work', async () => {
  mockCurrenciesRepository.getAll.mockImplementationOnce(() =>
    Promise.resolve([
      {
        name: 'name-1',
        symbol: 'symbol-1',
        code: 'code-1',
      },
      {
        name: 'name-2',
        symbol: 'symbol-2',
        code: 'code-2',
      },
    ]),
  );

  const service = new CurrenciesService(mockCurrenciesRepository, mockCacheUtility, mockDatabaseUtility);
  const result = await service.getCurrencies();

  console.log('result', result);
});

it('should also work', async () => {
  mockCurrenciesRepository.getAllByCode.mockImplementationOnce(() =>
    Promise.resolve({
      name: 'name-1',
      symbol: 'symbol-1',
      code: 'code-1',
    }),
  );

  const service = new CurrenciesService(mockCurrenciesRepository, mockCacheUtility, mockDatabaseUtility);
  const result = await service.getCurrency('hello');

  console.log('result', result);
});
