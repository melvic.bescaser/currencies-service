export interface ICurrency {
  name: string;
  symbol: string;
  code: string;
}
