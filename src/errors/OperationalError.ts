export class OperationalError extends Error {
  override name = 'OperationalError';

  constructor(message: string) {
    super(message);
  }
}
