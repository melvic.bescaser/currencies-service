import { IDatabaseUtility } from '@interfaces/IDatabaseUtility';
import { ICacheUtility } from '@interfaces/ICacheUtility';

export const mockDatabaseUtility: jest.Mocked<IDatabaseUtility> = {
  setup: jest.fn(),
  connect: jest.fn(),
  disconnect: jest.fn(),
  query: jest.fn(),
  setStore: jest.fn(),
  runTransaction: jest.fn().mockImplementation(async (fn) => await fn()),
};

export const mockCacheUtility: jest.Mocked<ICacheUtility> = {
  setup: jest.fn(),
  connect: jest.fn(),
  disconnect: jest.fn(),
  get: jest.fn(),
  set: jest.fn(),
  exists: jest.fn(),
};
