import { ICurrenciesRepository } from '@interfaces/ICurrenciesRepository';

export const mockCurrenciesRepository: jest.Mocked<ICurrenciesRepository> = {
  getAll: jest.fn(),
  getAllByCode: jest.fn(),
};
