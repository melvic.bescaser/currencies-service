import { injectable } from 'inversify';
import { get } from 'lodash';
import AWS from 'aws-sdk';

import { CONFIG_FACTORY_FUNCTION_TYPES } from '@configurations/constants';
import {
  IConfigUtility,
  ConfigRecord,
  ConfigFactory,
  ConfigFactoryFunctionTypeEnum,
  IParameterStoreResponse,
  KeyOf,
  Path,
  PathValue,
  ExcludeUndefinedIf,
  NoInferType,
  ConfigGetOptions,
  ParameterClientOptions,
} from '@interfaces/IConfigUtility';

@injectable()
export class ConfigUtility<K extends ConfigRecord = ConfigRecord, WasValidated extends boolean = true>
  implements IConfigUtility<K, WasValidated>
{
  private config: ConfigRecord = {};

  private client?: AWS.SSM;

  private shouldGetParameterFromEnv?: boolean;

  private isConfigLoaded = false;

  private isConfigAsyncLoaded = false;

  get<T = any>(key: KeyOf<K>): ExcludeUndefinedIf<WasValidated, T>;
  get<T = K, P extends Path<T> = any, R = PathValue<T, P>>(
    key: P,
    options: ConfigGetOptions,
  ): ExcludeUndefinedIf<WasValidated, R>;
  get<T = any>(key: KeyOf<K>, defaultValue: NoInferType<T>): T;
  get<T = K, P extends Path<T> = any, R = PathValue<T, P>>(
    propertyPath: P,
    defaultValue: NoInferType<R>,
    options: ConfigGetOptions,
  ): R;
  get(...args: any[]): any {
    const key = args[0];
    let defaultValue;

    if (args.length === 3 || (args.length === 2 && !(typeof args[1] === 'object' && 'infer' in args[1]))) {
      // eslint-disable-next-line prefer-destructuring
      defaultValue = args[1];
    }

    const isEnvKey = /^(?:[A-Z]{1}[A-Z0-9]*)(?:_[A-Z]{1}[A-Z0-9]*)*$/.test(key);

    if (!isEnvKey) {
      return get(this.config, key, defaultValue);
    }

    return get(process.env, key, defaultValue);
  }

  loadConfig(...configFactories: ConfigFactory<typeof CONFIG_FACTORY_FUNCTION_TYPES.SYNC>[]): void {
    if (this.isConfigLoaded) {
      return;
    }

    this.isConfigLoaded = true;
    this.checkHasConfigNameExists(configFactories);

    configFactories.forEach((configFactory) => {
      this.config[configFactory.KEY] = configFactory(this);
    });
  }

  async loadConfigAsync(
    ...configFactories: ConfigFactory<typeof CONFIG_FACTORY_FUNCTION_TYPES.ASYNC>[]
  ): Promise<void> {
    if (this.isConfigAsyncLoaded) {
      return;
    }

    this.isConfigAsyncLoaded = true;
    this.checkHasConfigNameExists(configFactories);

    await Promise.all(
      configFactories.map(async (configFactory) => {
        this.config[configFactory.KEY] = await configFactory(this);
      }),
    );
  }

  async getParameter(name: string, withDecryption?: boolean): Promise<IParameterStoreResponse> {
    try {
      if (this.shouldGetParameterFromEnv) {
        const [key] = name.split('/').reverse();
        const value = this.get<string>(key);

        if (value === undefined) {
          throw new Error(`Parameter name '${key}' not found.`);
        }

        if (value.trim() === '') {
          throw new Error(`Parameter name '${key}' value is empty.`);
        }

        return {
          name,
          value,
          version: 1,
        };
      }

      if (!this.client) {
        throw new Error(`Parameter client is not initialized.`);
      }

      const result = await this.client
        .getParameter({
          Name: name,
          WithDecryption: withDecryption,
        })
        .promise();

      if (!result.Parameter) {
        throw new Error(`Parameter name '${name}' has resolved but empty parameter object.`);
      }

      if (!result.Parameter.Name) {
        throw new Error(`Parameter name '${name}' name is empty.`);
      }

      if (!result.Parameter.Value) {
        throw new Error(`Parameter name '${name}' value is empty.`);
      }

      if (!result.Parameter.Version) {
        throw new Error(`Parameter name '${name}' version is empty.`);
      }

      if (result.Parameter.Type === 'SecureString' && !withDecryption) {
        throw new Error(`Parameter value of name '${name}' is encrypted, please enable 'withDecryption' mode.`);
      }

      return {
        name: result.Parameter.Name,
        version: result.Parameter.Version,
        value: result.Parameter.Value,
      };
    } catch (error) {
      if ((error as AWS.AWSError).code === 'ParameterNotFound') {
        throw new Error(`Parameter name '${name}' not found.`);
      }

      throw error;
    }
  }

  initializeParamter(options: ParameterClientOptions): void {
    if (this.client) {
      return;
    }

    this.client = new AWS.SSM(options);
  }

  setShouldGetParameterFromEnv(shouldGetParameterFromEnv: boolean): void {
    this.shouldGetParameterFromEnv = shouldGetParameterFromEnv;
  }

  private checkHasConfigNameExists(configFactories: ConfigFactory<ConfigFactoryFunctionTypeEnum>[]): void {
    const configNames = configFactories.map((configFactory) => configFactory.KEY);
    let foundConfig = configFactories.find((configFactory, index) => configNames.indexOf(configFactory.KEY) !== index);

    if (foundConfig) {
      throw new Error(`Duplicate config '${foundConfig.KEY}' name in list of configs.`);
    }

    foundConfig = configFactories.find((configFactory) => configFactory.KEY in this.config);

    if (foundConfig) {
      throw new Error(`Duplicate config '${foundConfig.KEY}' name in global configs.`);
    }
  }
}
