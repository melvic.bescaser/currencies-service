import { inject, injectable } from 'inversify';
import AWS from 'aws-sdk';

import { IDENTIFIERS } from '@configurations/constants';
import { IConfigUtility } from '@interfaces/IConfigUtility';
import { INotificationUtility } from '@interfaces/INotificationUtility';

@injectable()
export class NotificationUtility implements INotificationUtility {
  client: AWS.SNS;

  constructor(@inject(IDENTIFIERS.CONFIG_UTILITY) public configUtility: IConfigUtility) {
    this.client = new AWS.SNS({
      region: this.configUtility.get('AWS_DEFAULT_REGION'),
    });
  }

  async publish<T = any>(message: T, topic?: string, subject?: string): Promise<void> {
    const pubishMessage = typeof message === 'string' ? message : JSON.stringify(message);

    await this.client
      .publish({
        Message: pubishMessage,
        TopicArn: topic,
        Subject: subject,
      })
      .promise();
  }
}
