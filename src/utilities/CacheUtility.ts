import { inject, injectable } from 'inversify';
import { createClient } from 'redis';

import { IDENTIFIERS } from '@configurations/constants';
import { ICacheUtility, ISetOptions } from '@interfaces/ICacheUtility';
import { IConfigUtility } from '@interfaces/IConfigUtility';

@injectable()
export class CacheUtility implements ICacheUtility {
  public client: ReturnType<typeof createClient>;

  constructor(@inject(IDENTIFIERS.CONFIG_UTILITY) public configUtility: IConfigUtility) {}

  async setup() {
    if (this.client) {
      return;
    }

    this.client = createClient({
      url: this.configUtility.get('REDIS_URL'),
    });

    await this.connect();
  }

  async connect(): Promise<void> {
    await this.client.connect();
  }

  async disconnect(): Promise<void> {
    await this.client.quit();
  }

  async get(key: string): Promise<string | null> {
    return await this.client.get(key);
  }

  async set(key: string, value: string, options?: ISetOptions): Promise<void> {
    await this.client.set(key, value, options);
  }

  async exists(key: string): Promise<boolean> {
    return await this.client.exists(key);
  }
}
