import { inject, injectable } from 'inversify';
import AWS from 'aws-sdk';

import { IDENTIFIERS } from '@configurations/constants';
import { IConfigUtility } from '@interfaces/IConfigUtility';
import {
  IStorageUtility,
  IGetObjectRequestOptions,
  IGetObjectResponse,
  Body,
  IPutObjectRequestOptions,
} from '@interfaces/IStorageUtility';

@injectable()
export class StorageUtility implements IStorageUtility {
  client: AWS.S3;

  constructor(@inject(IDENTIFIERS.CONFIG_UTILITY) public configUtility: IConfigUtility) {
    this.client = new AWS.S3({
      region: this.configUtility.get('AWS_DEFAULT_REGION'),
    });
  }

  async getObject(bucket: string, key: string, options: IGetObjectRequestOptions = {}): Promise<IGetObjectResponse> {
    try {
      const result = await this.client
        .getObject({
          Bucket: bucket,
          Key: key,
          ResponseContentType: options.responseContentType,
        })
        .promise();

      return {
        body: result.Body as IGetObjectResponse['body'],
        contentType: result.ContentType,
      };
    } catch (error) {
      if ((error as AWS.AWSError).code === 'NoSuchKey') {
        throw new Error(`Key '${key}' does not exist on '${bucket}' bucket.`);
      }

      if ((error as AWS.AWSError).code === 'NoSuchBucket') {
        throw new Error(`Bucket '${bucket}' does not exist.`);
      }

      throw error;
    }
  }

  async putObject(bucket: string, key: string, body?: Body, options: IPutObjectRequestOptions = {}): Promise<void> {
    try {
      await this.client
        .putObject({
          Bucket: bucket,
          Key: key,
          Body: body,
          ContentType: options.contentType,
        })
        .promise();
    } catch (error) {
      if ((error as AWS.AWSError).code === 'NoSuchKey') {
        throw new Error(`Key '${key}' does not exist on '${bucket}' bucket.`);
      }

      if ((error as AWS.AWSError).code === 'NoSuchBucket') {
        throw new Error(`Bucket '${bucket}' does not exist.`);
      }

      throw error;
    }
  }
}
