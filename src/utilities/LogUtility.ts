import { injectable } from 'inversify';
import { Logger, createLogger, transports } from 'winston';

import { ILogUtility } from '@interfaces/ILogUtility';

@injectable()
export class LogUtility implements ILogUtility {
  public logger: Logger;

  async setup(): Promise<void> {
    if (this.logger) {
      return;
    }

    this.logger = createLogger({
      transports: [new transports.Console()],
    });
  }

  error(error: Error | string): void {
    if (error instanceof Error) {
      const options = {
        message: error.message,
        name: error.name,
        stack: error.stack,
      };

      this.logger.error(options);

      return;
    }

    this.logger.error(error);
  }

  info(message: string): void {
    this.logger.info(message);
  }
}
