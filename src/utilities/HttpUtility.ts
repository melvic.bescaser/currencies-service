import { injectable } from 'inversify';
import axios, { AxiosInstance } from 'axios';

import { IHttpUtility, IHttpRequestConfig, IHttpResponse } from '@interfaces/IHttpUtility';

@injectable()
export class HttpUtility implements IHttpUtility {
  public axiosInstance: AxiosInstance;

  constructor() {
    this.axiosInstance = axios.create();
  }

  get<T = any, R = any>(url: string, config?: IHttpRequestConfig<R>): Promise<IHttpResponse<T, R>> {
    return this.axiosInstance.get<T>(url, config);
  }

  delete<T = any, R = any>(url: string, config?: IHttpRequestConfig<R>): Promise<IHttpResponse<T, R>> {
    return this.axiosInstance.delete<T>(url, config);
  }

  head<T = any, R = any>(url: string, config?: IHttpRequestConfig<R>): Promise<IHttpResponse<T, R>> {
    return this.axiosInstance.head<T>(url, config);
  }

  options<T = any, R = any>(url: string, config?: IHttpRequestConfig<R>): Promise<IHttpResponse<T, R>> {
    return this.axiosInstance.options<T>(url, config);
  }

  post<T = any, R = any>(url: string, data?: R, config?: IHttpRequestConfig<R>): Promise<IHttpResponse<T, R>> {
    return this.axiosInstance.post(url, data, config);
  }

  put<T = any, R = any>(url: string, data?: R, config?: IHttpRequestConfig<R>): Promise<IHttpResponse<T, R>> {
    return this.axiosInstance.put(url, data, config);
  }

  patch<T = any, R = any>(url: string, data?: R, config?: IHttpRequestConfig<R>): Promise<IHttpResponse<T, R>> {
    return this.axiosInstance.patch(url, data, config);
  }

  request<T = any, R = any>(config: IHttpRequestConfig<R>): Promise<IHttpResponse<T, R>> {
    return this.axiosInstance.request(config);
  }
}
