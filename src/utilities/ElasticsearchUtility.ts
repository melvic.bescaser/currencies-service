import { injectable } from 'inversify';
import { Client, RequestParams, ApiResponse } from '@elastic/elasticsearch';
import type { TransportRequestOptions, RequestBody, RequestNDBody } from '@elastic/elasticsearch/lib/Transport';
import type {
  SearchResponse,
  CountResponse,
  MSearchResponse as MsearchResponse,
  GetResponse,
  UpdateDocumentByQueryResponse,
} from 'elasticsearch';
import { BulkHelperOptions, BulkStats } from '@elastic/elasticsearch/lib/Helpers';

import { IElasticsearchUtility, IBulkResponse } from '@interfaces/IElasticsearchUtility';

@injectable()
export class ElasticsearchUtility implements IElasticsearchUtility {
  client: Client;

  async setup(): Promise<void> {
    if (this.client) {
      return;
    }

    this.client = new Client({
      node: 'http://127.0.0.1:9300',
    });

    await this.ping(undefined, { requestTimeout: 3000 });
  }

  ping<C = unknown>(params?: RequestParams.Ping, options?: TransportRequestOptions): Promise<ApiResponse<boolean, C>> {
    return this.client.ping(params, options);
  }

  search<T = unknown, R extends RequestBody = Record<string, any>, C = unknown>(
    params: RequestParams.Search<R>,
    options?: TransportRequestOptions,
  ): Promise<ApiResponse<SearchResponse<T>, C>> {
    return this.client.search(params, options);
  }

  count<R extends RequestBody = Record<string, any>, C = unknown>(
    params: RequestParams.Count<R>,
    options?: TransportRequestOptions,
  ): Promise<ApiResponse<CountResponse, C>> {
    return this.client.count(params, options);
  }

  msearch<T = unknown, R extends RequestNDBody = Record<string, any>[], C = unknown>(
    params: RequestParams.Msearch<R>,
    options?: TransportRequestOptions,
  ): Promise<ApiResponse<MsearchResponse<T>, C>> {
    return this.client.msearch(params, options);
  }

  get<T = unknown, C = unknown>(
    params: RequestParams.Get,
    options?: TransportRequestOptions,
  ): Promise<ApiResponse<GetResponse<T>, C>> {
    return this.client.get(params, options);
  }

  exists<C = unknown>(
    params: RequestParams.Exists,
    options?: TransportRequestOptions,
  ): Promise<ApiResponse<boolean, C>> {
    return this.client.exists(params, options);
  }

  update<R extends RequestBody = Record<string, any>, C = unknown>(
    params: RequestParams.Update<R>,
    options?: TransportRequestOptions,
  ): Promise<ApiResponse<void, C>> {
    return this.client.update(params, options);
  }

  updateByQuery<R extends RequestBody = Record<string, any>, C = unknown>(
    params: RequestParams.UpdateByQuery<R>,
    options?: TransportRequestOptions,
  ): Promise<ApiResponse<UpdateDocumentByQueryResponse, C>> {
    return this.client.updateByQuery(params, options);
  }

  delete<C = unknown>(params: RequestParams.Delete, options?: TransportRequestOptions): Promise<ApiResponse<void, C>> {
    return this.client.delete(params, options);
  }

  bulk<R extends RequestNDBody = Record<string, any>[], C = unknown>(
    params: RequestParams.Bulk<R>,
    options?: TransportRequestOptions,
  ): Promise<ApiResponse<IBulkResponse, C>> {
    return this.client.bulk(params, options);
  }

  bulkHelper<T = unknown>(options: BulkHelperOptions<T>): Promise<BulkStats> {
    return this.client.helpers.bulk<T>(options);
  }
}
