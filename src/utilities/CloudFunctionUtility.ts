import { inject, injectable } from 'inversify';
import AWS from 'aws-sdk';

import { IDENTIFIERS } from '@configurations/constants';
import { IConfigUtility } from '@interfaces/IConfigUtility';
import { ICloudFunctionUtility, InvokeResponse } from '@interfaces/ICloudFunctionUtility';
import { JsonHelper } from '@helpers/JsonHelper';

@injectable()
export class CloudFunctionUtility implements ICloudFunctionUtility {
  client: AWS.Lambda;

  constructor(@inject(IDENTIFIERS.CONFIG_UTILITY) public configUtility: IConfigUtility) {
    this.client = new AWS.Lambda({
      region: this.configUtility.get('AWS_DEFAULT_REGION'),
    });
  }

  async invoke<T = any, P = any>(name: string, payload?: P): Promise<InvokeResponse<T>> {
    let invokePayload: string | undefined;

    if (payload !== undefined) {
      invokePayload = typeof payload === 'string' ? payload : JSON.stringify(payload);
    }

    const result = await this.client.invoke({ FunctionName: name, Payload: invokePayload }).promise();

    if (!result.Payload) {
      throw new Error(`Function '${name}' has empty response.`);
    }

    return JsonHelper.parse<InvokeResponse<T>>(result.Payload as string);
  }
}
