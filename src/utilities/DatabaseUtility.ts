import { inject, injectable } from 'inversify';
import ServerlessClient from 'serverless-postgres';

import { IDENTIFIERS } from '@configurations/constants';
import { ParameterStoreConfig } from '@core/ParameterStoreConfig';
import { DatabaseTransaction } from '@core/DatabaseTransaction';
import { IDatabaseUtility, IDatabaseTransactionStore, QueryConfig, QueryResult } from '@interfaces/IDatabaseUtility';
import { IConfigUtility, IParameterStoreParam } from '@interfaces/IConfigUtility';
import { ConfigHelper } from '@helpers/ConfigHelper';

type ConfigUtilityType = IConfigUtility<
  {
    STAGE_NAME: string;
    DB_USERNAME: string;
    DB_PASSWORD: string;
    DB_HOST: string;
    DB_PORT: string;
    DB_NAME: string;
  },
  true
>;

@injectable()
export class DatabaseUtility extends ParameterStoreConfig<ConfigUtilityType> implements IDatabaseUtility {
  public connection: ServerlessClient;

  public store?: IDatabaseTransactionStore;

  private dbName: string;

  constructor(@inject(IDENTIFIERS.CONFIG_UTILITY) configUtility: ConfigUtilityType) {
    super(configUtility);

    this.dbName = `/apollo/apollo-transactions-service/${configUtility.get(
      'STAGE_NAME',
      ConfigHelper.inferType,
    )}/DB_NAME`;
  }

  protected getParameterStoreParams(): IParameterStoreParam[] {
    return [
      {
        name: this.dbName,
        withDecryption: true,
      },
    ];
  }

  override async setup(): Promise<void> {
    await super.setup();

    if (this.connection && !this.checkIsParameterStoreConfigUpdated()) {
      return;
    }

    this.connection = new ServerlessClient({
      user: this.configUtility.get('DB_USERNAME', ConfigHelper.inferType),
      password: this.configUtility.get('DB_PASSWORD', ConfigHelper.inferType),
      host: this.configUtility.get('DB_HOST', ConfigHelper.inferType),
      port: this.configUtility.get('DB_PORT', ConfigHelper.inferType),
      database: this.getParameterStoreConfigByName(this.dbName).value,
    });
  }

  query<T, V extends any[] = any[]>(queryConfig: QueryConfig<V>): Promise<QueryResult<T>>;

  query<T>(queryConfig: string): Promise<QueryResult<T>>;

  async query<T, V extends any[] = any[]>(queryConfig: QueryConfig<V> | string): Promise<QueryResult<T>> {
    if (this.store && this.store.getStore()) {
      return await this.connection.query(queryConfig);
    }

    await this.connect();

    try {
      return await this.connection.query(queryConfig);
    } finally {
      await this.disconnect();
    }
  }

  async connect(): Promise<void> {
    await this.connection.connect();
  }

  async disconnect(): Promise<void> {
    await this.connection.clean();
  }

  setStore(store: IDatabaseTransactionStore): void {
    this.store = store;
  }

  async runTransaction<T>(fn: () => Promise<T>): Promise<T> {
    if (!this.store) {
      throw new Error(`Failed to use this method, you should first set 'store' value to use this.`);
    }

    const databaseTransactionFromStore = this.store.getStore();

    if (databaseTransactionFromStore) {
      return await fn();
    }

    const databaseTransaction = new DatabaseTransaction(this);

    return await this.store.run(databaseTransaction, async () => {
      await this.connect();

      try {
        await databaseTransaction.begin();

        const result = await fn();

        await databaseTransaction.commit();

        return result;
      } catch (error) {
        await databaseTransaction.rollback();

        throw error;
      } finally {
        await this.disconnect();
      }
    });
  }
}
