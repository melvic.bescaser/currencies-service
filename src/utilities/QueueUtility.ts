import { inject, injectable } from 'inversify';
import AWS from 'aws-sdk';

import { IDENTIFIERS } from '@configurations/constants';
import { IConfigUtility } from '@interfaces/IConfigUtility';
import { IMessage, IQueueUtility } from '@interfaces/IQueueUtility';

@injectable()
export class QueueUtility implements IQueueUtility {
  client: AWS.SQS;

  constructor(@inject(IDENTIFIERS.CONFIG_UTILITY) public configUtility: IConfigUtility) {
    this.client = new AWS.SQS({
      region: this.configUtility.get('AWS_DEFAULT_REGION'),
    });
  }

  async send<T = any>(url: string, payload: IMessage<T>): Promise<void> {
    const body = typeof payload.body === 'string' ? payload.body : JSON.stringify(payload.body);

    await this.client
      .sendMessage({
        QueueUrl: url,
        MessageBody: body,
        DelaySeconds: payload.delaySeconds,
        MessageAttributes: payload.messageAttributes,
        MessageDeduplicationId: payload.messageDeduplicationId,
        MessageGroupId: payload.messageGroupId,
      })
      .promise();
  }
}
