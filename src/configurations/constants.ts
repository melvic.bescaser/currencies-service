import { ResponseStatus } from './types';

export const RESPONSE_STATUS: ResponseStatus = {
  SUCCESS: 'success',
  ERROR: 'error',
};

export const METATADA_KEYS = {
  CONTROLLER: 'core:controller',
  CONTROLLER_ACTIONS: 'core:controller-actions',
};

export const IDENTIFIERS = {
  DATABASE_UTILITY: Symbol.for('DatabaseUtility'),
  CACHE_UTILITY: Symbol.for('CacheUtility'),
  LOG_UTILITY: Symbol.for('LogUtility'),
  CONFIG_UTILITY: Symbol.for('ConfigUtility'),
  HTTP_UTILITY: Symbol.for('HttpUtility'),
  ELASTICSEARCH_UTILITY: Symbol.for('ElasticsearchUtility'),
  QUEUE_UTILITY: Symbol.for('QueueUtility'),
  NOTIFICATION_UTILITY: Symbol.for('NotificationUtility'),
  STORAGE_UTILITY: Symbol.for('StorageUtility'),
  CLOUD_FUNCTION_UTILITY: Symbol.for('CloudFunctionUtility'),
  FOREX_CONTROLLER: Symbol.for('ForexController'),
  CURRENCIES_SERVICE: Symbol.for('CurrenciesService'),
  CURRENCIES_REPOSITORY: Symbol.for('CurrenciesRepository'),
  FOREIGN_EXCHANGES_REPOSITORY: Symbol.for('ForeignExchangesRepository'),
  CONTROLLER: Symbol.for('Controller'),
};

export const CONFIG_FACTORY_FUNCTION_TYPES = {
  SYNC: 'sync',
  ASYNC: 'async',
} as const;
