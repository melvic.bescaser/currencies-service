import type { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import { interfaces } from 'inversify';

export type ResponseStatus = {
  SUCCESS: 'success';
  ERROR: 'error';
};

type EventWithAction = { action: string };
export type APIGatewayProxyEventWithAction = APIGatewayProxyEvent & EventWithAction;

type SuccessResult<T> = {
  status: ResponseStatus['SUCCESS'];
  data?: T;
};
type ErrorResult = {
  status: ResponseStatus['ERROR'];
  message?: string;
};
type ResultWithStatus<T> = SuccessResult<T> | ErrorResult;
export type APIGatewayProxyResultWithStatus<T = unknown> = Omit<Omit<APIGatewayProxyResult, 'statusCode'>, 'body'> &
  ResultWithStatus<T>;

export type APIGatewayProxyEventWithBody<T = unknown> = Omit<APIGatewayProxyEvent, 'body'> & { body: T };

export type Controller = new (...any: any[]) => any;

export type Action = {
  name: string;
  controllerName: string;
  methodName: string;
};

export type ConfigFunction = (container: interfaces.Container) => Promise<void> | void;

export type HealthStatus = 'healthy' | 'unhealthy';

export type ValueOf<T> = T[keyof T];
