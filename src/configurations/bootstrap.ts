import { DatabaseTransactionStore } from '@core/DatabaseTransactionStore';
import { IDatabaseUtility } from '@interfaces/IDatabaseUtility';
import { IConfigUtility } from '@interfaces/IConfigUtility';
import { ICacheUtility } from '@interfaces/ICacheUtility';
import { ILogUtility } from '@interfaces/ILogUtility';
import { IElasticsearchUtility } from '@interfaces/IElasticsearchUtility';

import { ConfigFunction } from './types';
import { IDENTIFIERS } from './constants';

export const bootstrap: ConfigFunction = async (container) => {
  const configUtility = container.get<IConfigUtility>(IDENTIFIERS.CONFIG_UTILITY);

  configUtility.initializeParamter({
    region: process.env.AWS_DEFAULT_REGION,
  });

  const databaseUtility = container.get<IDatabaseUtility>(IDENTIFIERS.DATABASE_UTILITY);

  databaseUtility.setStore(new DatabaseTransactionStore());

  const cacheUtility = container.get<ICacheUtility>(IDENTIFIERS.CACHE_UTILITY);
  const logUtility = container.get<ILogUtility>(IDENTIFIERS.LOG_UTILITY);
  const elasticsearchUtility = container.get<IElasticsearchUtility>(IDENTIFIERS.ELASTICSEARCH_UTILITY);

  await Promise.all([databaseUtility.setup(), cacheUtility.setup(), logUtility.setup(), elasticsearchUtility.setup()]);
};
