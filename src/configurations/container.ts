import { Container } from 'inversify';

import { IConfigUtility } from '@interfaces/IConfigUtility';
import { IDatabaseUtility } from '@interfaces/IDatabaseUtility';
import { ICacheUtility } from '@interfaces/ICacheUtility';
import { ILogUtility } from '@interfaces/ILogUtility';
import { IHttpUtility } from '@interfaces/IHttpUtility';
import { IElasticsearchUtility } from '@interfaces/IElasticsearchUtility';
import { IQueueUtility } from '@interfaces/IQueueUtility';
import { INotificationUtility } from '@interfaces/INotificationUtility';
import { IStorageUtility } from '@interfaces/IStorageUtility';
import { ICloudFunctionUtility } from '@interfaces/ICloudFunctionUtility';
import { ICurrenciesRepository } from '@interfaces/ICurrenciesRepository';
import { IForeignExchangesRepository } from '@interfaces/IForeignExchangesRepository';
import { ICurrenciesService } from '@interfaces/ICurrenciesService';
import { ConfigUtility } from '@utilities/ConfigUtility';
import { DatabaseUtility } from '@utilities/DatabaseUtility';
import { CacheUtility } from '@utilities/CacheUtility';
import { LogUtility } from '@utilities/LogUtility';
import { HttpUtility } from '@utilities/HttpUtility';
import { ElasticsearchUtility } from '@utilities/ElasticsearchUtility';
import { QueueUtility } from '@utilities/QueueUtility';
import { NotificationUtility } from '@utilities/NotificationUtility';
import { StorageUtility } from '@utilities/StorageUtility';
import { CloudFunctionUtility } from '@utilities/CloudFunctionUtility';
import { CurrenciesRepository } from '@repositories/CurrenciesRepository';
import { ForeignExchangesRepository } from '@repositories/ForeignExchangesRepository';
import { CurrenciesService } from '@services/CurrenciesService';

import { IDENTIFIERS } from './constants';

export const container = new Container();

container.bind<IConfigUtility>(IDENTIFIERS.CONFIG_UTILITY).to(ConfigUtility).inSingletonScope();
container.bind<IDatabaseUtility>(IDENTIFIERS.DATABASE_UTILITY).to(DatabaseUtility).inSingletonScope();
container.bind<ICacheUtility>(IDENTIFIERS.CACHE_UTILITY).to(CacheUtility).inSingletonScope();
container.bind<ILogUtility>(IDENTIFIERS.LOG_UTILITY).to(LogUtility).inSingletonScope();
container.bind<IHttpUtility>(IDENTIFIERS.HTTP_UTILITY).to(HttpUtility).inSingletonScope();
container.bind<IElasticsearchUtility>(IDENTIFIERS.ELASTICSEARCH_UTILITY).to(ElasticsearchUtility).inSingletonScope();
container.bind<IQueueUtility>(IDENTIFIERS.QUEUE_UTILITY).to(QueueUtility).inSingletonScope();
container.bind<INotificationUtility>(IDENTIFIERS.NOTIFICATION_UTILITY).to(NotificationUtility).inSingletonScope();
container.bind<IStorageUtility>(IDENTIFIERS.STORAGE_UTILITY).to(StorageUtility).inSingletonScope();
container.bind<ICloudFunctionUtility>(IDENTIFIERS.CLOUD_FUNCTION_UTILITY).to(CloudFunctionUtility).inSingletonScope();

container.bind<ICurrenciesRepository>(IDENTIFIERS.CURRENCIES_REPOSITORY).to(CurrenciesRepository);
container.bind<IForeignExchangesRepository>(IDENTIFIERS.FOREIGN_EXCHANGES_REPOSITORY).to(ForeignExchangesRepository);

container.bind<ICurrenciesService>(IDENTIFIERS.CURRENCIES_SERVICE).to(CurrenciesService);
