import { injectable, inject } from 'inversify';

import { IDENTIFIERS } from '@configurations/constants';
import { ICurrenciesRepository } from '@interfaces/ICurrenciesRepository';
import { IDatabaseUtility } from '@interfaces/IDatabaseUtility';
import { DatabaseHelper } from '@helpers/DatabaseHelper';
import { ICurrency } from '@models/ICurrencies';

@injectable()
export class CurrenciesRepository implements ICurrenciesRepository {
  constructor(
    @inject(IDENTIFIERS.DATABASE_UTILITY)
    public databaseUtility: IDatabaseUtility,
  ) {}

  async getAll(): Promise<ICurrency[]> {
    const preparedStatement = DatabaseHelper.queryBuilder.select('*').from({ u: 'apollo.currencies' });
    const queryConfig = DatabaseHelper.buildQueryConfig(preparedStatement, 'get-all-currencies');
    const result = await this.databaseUtility.query(queryConfig);

    return result.rows.map<ICurrency>((row) => ({
      name: row.name,
      symbol: row.symbol,
      code: row.code,
    }));
  }

  async getAllByCode(code: string): Promise<ICurrency | null> {
    const queryConfig = {
      name: 'get-all-currencies-by-code',
      text: 'SELECT * FROM apollo.currencies c WHERE LOWER(c.code) = LOWER($1)',
      values: [code],
    };
    const result = await this.databaseUtility.query(queryConfig);

    if (result.rows.length === 0) {
      return null;
    }

    const [row] = result.rows;

    return {
      name: row.name,
      symbol: row.symbol,
      code: row.code,
    };
  }
}
