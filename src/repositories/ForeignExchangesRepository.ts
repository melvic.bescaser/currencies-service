import { inject, injectable } from 'inversify';

import { IDENTIFIERS } from '@configurations/constants';
import { IDatabaseUtility } from '@interfaces/IDatabaseUtility';
import { IForeignExchangesRepository } from '@interfaces/IForeignExchangesRepository';
import { IForeignExchange } from '@models/IForeignExchange';

@injectable()
export class ForeignExchangesRepository implements IForeignExchangesRepository {
  constructor(
    @inject(IDENTIFIERS.DATABASE_UTILITY)
    public databaseUtility: IDatabaseUtility,
  ) {}

  async getAll(): Promise<IForeignExchange[]> {
    return [...Array(5)].map<IForeignExchange>((_, i) => ({
      rate: `${(i + 1) * 100}.00`,
    }));
  }
}
