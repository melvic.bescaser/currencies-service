import { mockDatabaseUtility } from '@mocks/utilities';

import { CurrenciesRepository } from './CurrenciesRepository';

beforeEach(() => {
  jest.clearAllMocks();
});

it('should work', async () => {
  mockDatabaseUtility.query.mockImplementationOnce(() =>
    Promise.resolve({
      oid: 0,
      command: '',
      fields: [],
      rowCount: 2,
      rows: [
        {
          name: 'name-1',
          symbol: 'symbol-1',
          code: 'code-1',
        },
        {
          name: 'name-2',
          symbol: 'symbol-2',
          code: 'code-2',
        },
      ],
    }),
  );

  const repository = new CurrenciesRepository(mockDatabaseUtility);
  const result = await repository.getAll();

  console.log('result', result);
});

it('should also work', async () => {
  mockDatabaseUtility.query.mockImplementationOnce(() =>
    Promise.resolve({
      oid: 0,
      command: '',
      fields: [],
      rowCount: 1,
      rows: [
        {
          name: 'name-1',
          symbol: 'symbol-1',
          code: 'code-1',
        },
      ],
    }),
  );

  const repository = new CurrenciesRepository(mockDatabaseUtility);
  const result = await repository.getAllByCode('hello');

  console.log('result', result);
});
