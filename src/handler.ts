import 'reflect-metadata';
import 'source-map-support/register';
import { Context } from 'aws-lambda';

import { container } from './configurations/container';
import { bootstrap } from './configurations/bootstrap';
import { EventAction } from './core/EventAction';
import type { APIGatewayProxyEventWithAction, APIGatewayProxyResultWithStatus } from './configurations/types';
import { ResponseHelper } from './helpers/ResponseHelper';
import { CurrenciesController } from './controllers/CurrenciesController';

const eventAction = new EventAction(container);

eventAction.setConfig(bootstrap).register(CurrenciesController);

export const main = async (
  event: APIGatewayProxyEventWithAction,
  context: Context,
): Promise<APIGatewayProxyResultWithStatus> => {
  try {
    const result = await eventAction.run(event, context);

    return result;
  } catch (error) {
    return ResponseHelper.handleError(error);
  }
};
