import { injectable, unmanaged } from 'inversify';

import {
  IConfigUtility,
  IParameterStoreParam,
  IParameterStoreResponse,
  IParameterStore,
} from '@interfaces/IConfigUtility';

@injectable()
export abstract class ParameterStoreConfig<T extends IConfigUtility<any, boolean>> {
  protected configUtility: T;

  private oldParameterStore?: Partial<IParameterStore>;

  private newParameterStore?: Partial<IParameterStore>;

  constructor(@unmanaged() configUtility: T) {
    this.configUtility = configUtility;
  }

  protected abstract getParameterStoreParams(): IParameterStoreParam[];

  protected async setup(): Promise<void> {
    const parameterStoreParams = this.getParameterStoreParams();

    if (parameterStoreParams.length === 0) {
      throw new Error('Parameter store params should have items to fetch.');
    }

    const promises = parameterStoreParams.map<Promise<[string, IParameterStoreResponse]>>(
      async (parameterStoreParam) => {
        const parameterStoreResponse = await this.configUtility.getParameter(
          parameterStoreParam.name,
          parameterStoreParam.withDecryption,
        );

        return [parameterStoreParam.name, parameterStoreResponse];
      },
    );
    const result = await Promise.all(promises);

    this.oldParameterStore = this.newParameterStore;
    this.newParameterStore = Object.fromEntries(result);
  }

  protected checkIsParameterStoreConfigUpdated(): boolean {
    const { oldParameterStore } = this;
    const { newParameterStore } = this;

    if (!oldParameterStore && newParameterStore) {
      return true;
    }

    if (!(oldParameterStore && newParameterStore)) {
      throw new Error('Parameter store is not defined.');
    }

    return Object.values(newParameterStore as IParameterStore).some((parameterStore) => {
      const foundParameterStore = oldParameterStore[parameterStore.name];

      if (foundParameterStore) {
        return foundParameterStore.version !== parameterStore.version;
      }

      return true;
    });
  }

  protected checkIsParameterStoreConfigUpdatedByName(name: string): boolean {
    const { oldParameterStore } = this;
    const { newParameterStore } = this;

    if (!oldParameterStore && newParameterStore) {
      const value = newParameterStore[name];

      if (!value) {
        throw new Error(`Parameter name '${name}' is missing in store.`);
      }

      return true;
    }

    if (!(oldParameterStore && newParameterStore)) {
      throw new Error('Parameter store is not defined.');
    }

    const oldValue = oldParameterStore[name];
    const newValue = newParameterStore[name];

    if (!oldValue && newValue) {
      return true;
    }

    if (!(oldValue && newValue)) {
      throw new Error(`Parameter name '${name}' is missing in store.`);
    }

    return oldValue.version !== newValue.version;
  }

  protected getParameterStoreConfigByName(name: string): IParameterStoreResponse {
    const store = this.newParameterStore;

    if (!store) {
      throw new Error('Parameter store is not defined.');
    }

    const value = store[name];

    if (!value) {
      throw new Error(`Parameter name '${name}' is missing in store.`);
    }

    return value;
  }
}
