import { IDatabaseTransaction, IDatabaseUtility } from '@interfaces/IDatabaseUtility';

export class DatabaseTransaction implements IDatabaseTransaction {
  constructor(public databaseUtility: IDatabaseUtility) {}

  async begin(): Promise<void> {
    await this.databaseUtility.query('BEGIN');
  }

  async commit(): Promise<void> {
    await this.databaseUtility.query('COMMIT');
  }

  async rollback(): Promise<void> {
    await this.databaseUtility.query('ROLLBACK');
  }
}
