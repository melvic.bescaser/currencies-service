import { AsyncLocalStorage } from 'async_hooks';

import { IDatabaseTransactionStore, IDatabaseTransaction } from '@interfaces/IDatabaseUtility';

export class DatabaseTransactionStore implements IDatabaseTransactionStore {
  public store: AsyncLocalStorage<IDatabaseTransaction>;

  constructor() {
    this.store = new AsyncLocalStorage<IDatabaseTransaction>();
  }

  getStore(): IDatabaseTransaction | undefined {
    return this.store.getStore();
  }

  async run<T>(value: IDatabaseTransaction, fn: () => Promise<T>): Promise<T> {
    return await this.store.run(value, fn);
  }
}
