import { decorate, injectable } from 'inversify';

import { METATADA_KEYS } from '@configurations/constants';

export const controller = (/* ...middlewares?: Middleware[] */) => (target: any) => {
  decorate(injectable(), target);

  const metadata = {
    target,
    // middlewares,
  };

  Reflect.defineMetadata(METATADA_KEYS.CONTROLLER, metadata, target);
};

export const action = (actionName: string /* , ...middlewares?: Middleware[] */) => (target: any, key: string) => {
  const metadata = {
    target,
    key,
    action: actionName,
    // middlewares,
  };

  if (Reflect.hasOwnMetadata(METATADA_KEYS.CONTROLLER_ACTIONS, target.constructor)) {
    Reflect.getOwnMetadata(METATADA_KEYS.CONTROLLER_ACTIONS, target.constructor).push(metadata);
  } else {
    Reflect.defineMetadata(METATADA_KEYS.CONTROLLER_ACTIONS, [metadata], target.constructor);
  }
};
