import { interfaces } from 'inversify';
import { Context } from 'aws-lambda';

import { IDENTIFIERS, METATADA_KEYS } from '@configurations/constants';
import {
  Controller,
  Action,
  ConfigFunction,
  APIGatewayProxyEventWithAction,
  APIGatewayProxyResultWithStatus,
} from '@configurations/types';
import { IControllerMetadata, IControllerActionMetadata, IController } from '@interfaces/IController';

export class EventAction {
  actions: Partial<Record<string, Action>> = {};

  controllers: string[] = [];

  container: interfaces.Container;

  configFn?: ConfigFunction;

  constructor(container: interfaces.Container) {
    this.container = container;
  }

  setConfig(configFn: ConfigFunction) {
    this.configFn = configFn;

    return this;
  }

  register(controller: Controller) {
    const controllerMetadata = EventAction.getControllerMetadata(controller);

    if (!controllerMetadata) {
      throw new Error(`Controller '${controller.name}' should have '@controller' decorator.`);
    }

    const controllerActionsMetadata = EventAction.getControllerActionsMetadata(controller);

    if (!controllerActionsMetadata) {
      throw new Error(`Controller '${controller.name}' should at least have one method with '@action' decorator.`);
    }

    if (this.controllers.includes(controller.name)) {
      throw new Error(`Duplicate controller '${controller.name}' name.`);
    }

    const actions: Action[] = controllerActionsMetadata.map((controllerActionMetadata) => ({
      name: controllerActionMetadata.action,
      methodName: controllerActionMetadata.key,
      controllerName: controller.name,
    }));
    const actionNames = actions.map((action) => action.name);
    let duplicateAction = actions.find((action, index) => actionNames.indexOf(action.name) !== index);

    if (duplicateAction) {
      throw new Error(`Duplicate action '${duplicateAction.name}' name for '${controller.name}' controller.`);
    }

    duplicateAction = actions.find((action) => this.actions[action.name]);

    if (duplicateAction) {
      throw new Error(`Duplicate action '${duplicateAction.name}' name for global actions.`);
    }

    this.registerController(controller, actions);

    return this;
  }

  async run(event: APIGatewayProxyEventWithAction, context: Context): Promise<APIGatewayProxyResultWithStatus> {
    if (this.controllers.length === 0) {
      throw new Error('Controller events should at least have one registered controller with one or more actions.');
    }

    const action = this.actions[event.action];

    if (!action) {
      throw new Error(`Action '${event.action}' name not found.`);
    }

    const controller = this.container.getNamed<IController>(IDENTIFIERS.CONTROLLER, action.controllerName);

    if (this.configFn) {
      await this.configFn(this.container);
    }

    return controller[action.methodName](event, context);
  }

  private registerController(controller: Controller, actions: Action[]): void {
    this.container.bind<IController>(IDENTIFIERS.CONTROLLER).to(controller).whenTargetNamed(controller.name);
    this.controllers.push(controller.name);
    actions.forEach((action) => {
      this.actions[action.name] = action;
    });
  }

  static getControllerMetadata(controller: Controller): IControllerMetadata | undefined {
    return Reflect.getMetadata(METATADA_KEYS.CONTROLLER, controller);
  }

  static getControllerActionsMetadata(controller: Controller): IControllerActionMetadata[] | undefined {
    return Reflect.getOwnMetadata(METATADA_KEYS.CONTROLLER_ACTIONS, controller);
  }
}
