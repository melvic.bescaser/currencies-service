import { Knex, knex } from 'knex';

import { QueryConfig } from '@interfaces/IDatabaseUtility';

export class DatabaseHelper {
  static queryBuilder: Knex.QueryBuilder = knex({ client: 'pg' }).queryBuilder();

  static buildQueryConfig(queryBuilder: Knex.QueryBuilder, name?: string): QueryConfig {
    const preparedStatement = queryBuilder.toSQL().toNative();

    return {
      name,
      text: preparedStatement.sql,
      values: preparedStatement.bindings as QueryConfig['values'],
    };
  }
}
