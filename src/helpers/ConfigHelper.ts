import { CONFIG_FACTORY_FUNCTION_TYPES } from '@configurations/constants';
import {
  ConfigFactoryFunction,
  ConfigFactoryFunctionAsync,
  ConfigGetOptions,
  ConfigRecord,
  ConfigFactory,
} from '@interfaces/IConfigUtility';

export class ConfigHelper {
  static inferType: ConfigGetOptions = Object.freeze({ infer: true });

  static registerAs<T extends ConfigRecord, F extends ConfigFactoryFunction<T>>(
    name: string,
    fn: ReturnType<F> extends Promise<unknown> ? never : ConfigFactoryFunction<T>,
  ): ConfigFactory<typeof CONFIG_FACTORY_FUNCTION_TYPES.SYNC, T>;

  static registerAs<T extends ConfigRecord, F extends ConfigFactoryFunctionAsync<T>>(
    name: string,
    fn: ReturnType<F> extends Promise<unknown> ? ConfigFactoryFunctionAsync<T> : never,
  ): ConfigFactory<typeof CONFIG_FACTORY_FUNCTION_TYPES.ASYNC, T>;

  static registerAs(name: any, fn: any): any {
    const isEnvKey = /^(?:[A-Z]{1}[A-Z0-9]*)(?:_[A-Z]{1}[A-Z0-9]*)*$/.test(name);

    if (isEnvKey) {
      throw new Error("Config name should not be similar to environment variable's pattern.");
    }

    Object.assign(fn, { KEY: name });

    return fn;
  }
}
