import { RESPONSE_STATUS } from '@configurations/constants';
import type { APIGatewayProxyResultWithStatus } from '@configurations/types';

export class ResponseHelper {
  static handle<T>(data?: T): APIGatewayProxyResultWithStatus<T> {
    return { status: RESPONSE_STATUS.SUCCESS, data };
  }

  static handleError<T>(error: any): APIGatewayProxyResultWithStatus<T> {
    if (error instanceof Error) {
      return { status: RESPONSE_STATUS.ERROR, message: error.message };
    }

    return { status: RESPONSE_STATUS.ERROR, message: error };
  }
}
