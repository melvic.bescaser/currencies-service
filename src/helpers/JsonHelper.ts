export class JsonHelper {
  static parse<T>(value: string): T {
    try {
      return JSON.parse(value);
    } catch {
      throw new Error('Failed to parse given json string.');
    }
  }

  static stringify<T>(value: T extends string ? never : T, pretty?: boolean): string {
    if (pretty) {
      return JSON.stringify(value, null, 2);
    }

    return JSON.stringify(value);
  }
}
