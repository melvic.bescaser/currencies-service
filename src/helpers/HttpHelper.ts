import { IHttpError } from '@interfaces/IHttpUtility';

export class HttpHelper {
  static checkIsHttpError<T = any, R = any>(error: any): error is IHttpError<T, R> {
    return !!error?.isAxiosError;
  }
}
