import { CONFIG_FACTORY_FUNCTION_TYPES } from '@configurations/constants';
import { ValueOf } from '@configurations/types';

/* eslint-disable no-use-before-define */
export type ConfigFactoryFunctionTypeEnum = ValueOf<typeof CONFIG_FACTORY_FUNCTION_TYPES>;

export type ConfigRecord = Record<string, unknown>;

export type ConfigType<T extends ConfigFactory<ConfigFactoryFunctionTypeEnum>> = Awaited<ReturnType<T>>;

export type ConfigFactoryFunctionAsync<T> = (configUtility: IConfigUtility<ConfigRecord, boolean>) => Promise<T>;

export type ConfigFactoryFunction<T> = (configUtility: IConfigUtility<ConfigRecord, boolean>) => T;

type ConfigKey = { KEY: string };
export type ConfigFactory<
  K extends ConfigFactoryFunctionTypeEnum,
  T extends ConfigRecord = any,
  F extends ConfigFactoryFunction<T> | ConfigFactoryFunctionAsync<T> = any,
> = K extends typeof CONFIG_FACTORY_FUNCTION_TYPES.SYNC
  ? ReturnType<F> extends Promise<unknown>
    ? never
    : ConfigFactoryFunction<T> & ConfigKey
  : K extends typeof CONFIG_FACTORY_FUNCTION_TYPES.ASYNC
  ? ReturnType<F> extends Promise<unknown>
    ? ConfigFactoryFunctionAsync<T> & ConfigKey
    : never
  : never;

export interface IParameterStoreParam {
  name: string;
  withDecryption?: boolean;
}

export interface IParameterStoreResponse {
  value: string;
  version: number;
  name: string;
}

export interface IParameterStore {
  [key: string]: IParameterStoreResponse;
}

export interface IOnSetup {
  setup(): Promise<void>;
}

type PathImpl<T, Key extends keyof T> = Key extends string
  ? T[Key] extends Record<string, any>
    ?
        | `${Key}.${PathImpl<T[Key], Exclude<keyof T[Key], keyof any[]>> & string}`
        | `${Key}.${Exclude<keyof T[Key], keyof any[]> & string}`
    : never
  : never;
type PathImpl2<T> = PathImpl<T, keyof T> | keyof T;
export type Path<T> = keyof T extends string ? (PathImpl2<T> extends string | keyof T ? PathImpl2<T> : keyof T) : never;

export type PathValue<T, P extends Path<T>> = P extends `${infer Key}.${infer Rest}`
  ? Key extends keyof T
    ? Rest extends Path<T[Key]>
      ? PathValue<T[Key], Rest>
      : never
    : never
  : P extends keyof T
  ? T[P]
  : never;

export type NoInferType<T> = [T][T extends any ? 0 : never];

export type ExcludeUndefinedIf<ExcludeUndefined extends boolean, T> = ExcludeUndefined extends true
  ? Exclude<T, undefined>
  : T | undefined;

export type KeyOf<T> = keyof T extends never ? string : keyof T;

export type ConfigGetOptions = {
  infer: true;
};

export type ParameterClientOptions = {
  region?: string;
};

export interface IConfigUtility<K = ConfigRecord, WasValidated extends boolean = false> {
  get<T = any>(key: KeyOf<K>): ExcludeUndefinedIf<WasValidated, T>;
  get<T = K, P extends Path<T> = any, R = PathValue<T, P>>(
    key: P,
    options: ConfigGetOptions,
  ): ExcludeUndefinedIf<WasValidated, R>;
  get<T = any>(key: KeyOf<K>, defaultValue: NoInferType<T>): T;
  get<T = K, P extends Path<T> = any, R = PathValue<T, P>>(
    propertyPath: P,
    defaultValue: NoInferType<R>,
    options: ConfigGetOptions,
  ): R;
  getParameter(name: string, withDecryption?: boolean): Promise<IParameterStoreResponse>;
  loadConfig(...configFactories: ConfigFactory<typeof CONFIG_FACTORY_FUNCTION_TYPES.SYNC>[]): void;
  loadConfigAsync(...configFactories: ConfigFactory<typeof CONFIG_FACTORY_FUNCTION_TYPES.ASYNC>[]): Promise<void>;
  initializeParamter(options: ParameterClientOptions): void;
  setShouldGetParameterFromEnv(shouldGetParameterFromEnv: boolean): void;
}

// export interface IConfigUtility {
//   get<T = any>(key: string, defaultValue?: T): T | undefined;
//   setConfig(config: Record<string, any>): void;
// }
