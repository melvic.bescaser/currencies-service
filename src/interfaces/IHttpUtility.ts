type Method =
  | 'get'
  | 'GET'
  | 'delete'
  | 'DELETE'
  | 'head'
  | 'HEAD'
  | 'options'
  | 'OPTIONS'
  | 'post'
  | 'POST'
  | 'put'
  | 'PUT'
  | 'patch'
  | 'PATCH'
  | 'purge'
  | 'PURGE'
  | 'link'
  | 'LINK'
  | 'unlink'
  | 'UNLINK';

type ResponseType = 'arraybuffer' | 'blob' | 'document' | 'json' | 'text' | 'stream';

export interface IHttpRequestConfig<R = any> {
  url?: string;
  method?: Method;
  data?: R;
  headers?: Record<string, string>;
  params?: any;
  responseType?: ResponseType;
  paramsSerializer?: (params: any) => string;
}

export interface IHttpResponse<T = any, R = any> {
  data: T;
  status: number;
  statusText: string;
  headers: Record<string, string>;
  config: IHttpRequestConfig<R>;
  request?: any;
}

export interface IHttpError<T = any, R = any> extends Error {
  config: IHttpRequestConfig<R>;
  code?: string;
  request?: any;
  response?: IHttpResponse<T, R>;
}

export interface IHttpUtility {
  get<T = any, R = any>(url: string, config?: IHttpRequestConfig<R>): Promise<IHttpResponse<T, R>>;
  delete<T = any, R = any>(url: string, config?: IHttpRequestConfig<R>): Promise<IHttpResponse<T, R>>;
  head<T = any, R = any>(url: string, config?: IHttpRequestConfig<R>): Promise<IHttpResponse<T, R>>;
  options<T = any, R = any>(url: string, config?: IHttpRequestConfig<R>): Promise<IHttpResponse<T, R>>;
  post<T = any, R = any>(url: string, data?: R, config?: IHttpRequestConfig<R>): Promise<IHttpResponse<T, R>>;
  put<T = any, R = any>(url: string, data?: R, config?: IHttpRequestConfig<R>): Promise<IHttpResponse<T, R>>;
  patch<T = any, R = any>(url: string, data?: R, config?: IHttpRequestConfig<R>): Promise<IHttpResponse<T, R>>;
  patch<T = any, R = any>(url: string, data?: R, config?: IHttpRequestConfig<R>): Promise<IHttpResponse<T, R>>;
  request<T = any, R = any>(config: IHttpRequestConfig<R>): Promise<IHttpResponse<T, R>>;
}
