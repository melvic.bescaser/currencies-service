import { Controller } from '@configurations/types';

export interface IControllerMetadata {
  target: Controller;
}

export interface IControllerActionMetadata {
  action: string;
  key: string;
}

export interface IController {
  [key: string]: any;
}
