import { IForeignExchange } from '@models/IForeignExchange';

export interface IForeignExchangesRepository {
  getAll(): Promise<IForeignExchange[]>;
}
