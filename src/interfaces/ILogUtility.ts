import { OperationalError } from '@errors/OperationalError';

export interface ILogUtility {
  setup(): Promise<void>;
  error(message: string | OperationalError): void;
  info(message: string): void;
}
