export interface INotificationUtility {
  publish<T = any>(message: T, topic?: string, subject?: string): Promise<void>;
}
