import { Readable } from 'stream';

export type Body = Buffer | Uint8Array | string | Readable;

export interface IGetObjectRequestOptions {
  responseContentType?: string;
}

export interface IGetObjectResponse {
  body: Body | undefined;
  contentType?: string;
}

export interface IPutObjectRequestOptions {
  contentType?: string;
}

export interface IStorageUtility {
  getObject(bucket: string, key: string, options?: IGetObjectRequestOptions): Promise<IGetObjectResponse>;
  putObject(bucket: string, key: string, body?: Body, options?: IPutObjectRequestOptions): Promise<void>;
}
