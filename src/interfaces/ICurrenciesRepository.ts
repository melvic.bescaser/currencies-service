import { ICurrency } from '@models/ICurrencies';

export interface ICurrenciesRepository {
  getAll(): Promise<ICurrency[]>;
  getAllByCode(code: string): Promise<ICurrency | null>;
}
