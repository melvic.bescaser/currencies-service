export interface ISetOptions {
  EX?: number;
  NX?: number;
}

export interface ICacheUtility {
  setup(): Promise<void>;
  connect(): Promise<void>;
  disconnect(): Promise<void>;
  get(key: string): Promise<string | null>;
  set(key: string, value: string, options?: ISetOptions): Promise<void>;
  exists(key: string): Promise<boolean>;
}
