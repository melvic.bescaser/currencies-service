type StringList = string[];
type Binary = Buffer | Uint8Array | string;
type BinaryList = Binary[];

interface IMessageAttributeValue {
  StringValue?: string;
  BinaryValue?: Binary;
  StringListValues?: StringList;
  BinaryListValues?: BinaryList;
  DataType: string;
}
interface IMessageBodyAttributeMap {
  [key: string]: IMessageAttributeValue;
}

export interface IMessage<T = any> {
  body: T;
  messageDeduplicationId?: string;
  messageGroupId?: string;
  delaySeconds?: number;
  messageAttributes?: IMessageBodyAttributeMap;
}

export interface IQueueUtility {
  send<T = any>(url: string, payload: IMessage<T>): Promise<void>;
}
