type SuccessInvokeResponse<T> = { status: 'success'; data?: T };
type ErrorInvokeResponse = { status: 'error'; message?: string };

export type InvokeResponse<T = any> = SuccessInvokeResponse<T> | ErrorInvokeResponse;

export interface ICloudFunctionUtility {
  invoke<T = any, P = any>(name: string, payload?: P): Promise<InvokeResponse<T>>;
}
