export interface IDatabaseTransaction {
  begin(): Promise<void>;
  commit(): Promise<void>;
  rollback(): Promise<void>;
}
export interface IDatabaseTransactionStore {
  getStore(): IDatabaseTransaction | undefined;
  run<T>(value: IDatabaseTransaction, fn: () => Promise<T>): Promise<T>;
}

export interface QueryConfig<V extends any[] = any[]> {
  name?: string | undefined;
  text: string;
  values?: V | undefined;
}

export interface FieldDef {
  name: string;
  tableID: number;
  columnID: number;
  dataTypeID: number;
  dataTypeSize: number;
  dataTypeModifier: number;
  format: string;
}

export interface QueryResultBase {
  command: string;
  rowCount: number;
  oid: number;
  fields: FieldDef[];
}

export interface QueryResultRow {
  [column: string]: any;
}

export interface QueryResult<R extends QueryResultRow = any> extends QueryResultBase {
  rows: R[];
}

export interface IDatabaseUtility {
  setup(): Promise<void>;
  connect(): Promise<void>;
  disconnect(): Promise<void>;
  query<T = any, V extends any[] = any[]>(queryConfig: QueryConfig<V>): Promise<QueryResult<T>>;
  query<T = any>(queryConfig: string): Promise<QueryResult<T>>;
  setStore(store: IDatabaseTransactionStore): void;
  runTransaction<T>(fn: () => Promise<T>): Promise<T>;
}
