import { ICurrency } from '@models/ICurrencies';

export interface ICurrenciesService {
  getCurrencies(): Promise<ICurrency[]>;
  getCurrency(code: string): Promise<ICurrency>;
}
