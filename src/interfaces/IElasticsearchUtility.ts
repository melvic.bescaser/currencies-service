import type { ApiResponse } from '@elastic/elasticsearch';
import type { RequestBody, RequestNDBody, TransportRequestOptions } from '@elastic/elasticsearch/lib/Transport';
import type { BulkHelperOptions, BulkStats } from '@elastic/elasticsearch/lib/Helpers';
import { RequestParams } from '@elastic/elasticsearch';
import type {
  CountResponse,
  SearchResponse,
  MSearchResponse as MsearchResponse,
  GetResponse,
  UpdateDocumentByQueryResponse,
} from 'elasticsearch';

interface IErrorCause {
  type: string;
  reason: string;
  caused_by?: IErrorCause;
}

interface IBulkResponseItemBase {
  _id?: string | null;
  _index: string;
  status: number;
  error?: IErrorCause;
  result?: string;
}

interface IBulkResponseItemContainer {
  index?: IBulkResponseItemBase;
  create?: IBulkResponseItemBase;
  update?: IBulkResponseItemBase;
  delete?: IBulkResponseItemBase;
}

export interface IBulkResponse {
  errors: boolean;
  items: IBulkResponseItemContainer[];
  took: number;
  ingest_took?: number;
}

export interface IElasticsearchUtility {
  setup(): Promise<void>;
  ping<C = unknown>(params?: RequestParams.Ping, options?: TransportRequestOptions): Promise<ApiResponse<boolean, C>>;
  search<T = unknown, R extends RequestBody = Record<string, any>, C = unknown>(
    params: RequestParams.Search<R>,
    options?: TransportRequestOptions,
  ): Promise<ApiResponse<SearchResponse<T>, C>>;
  count<R extends RequestBody = Record<string, any>, C = unknown>(
    params: RequestParams.Count<R>,
    options?: TransportRequestOptions,
  ): Promise<ApiResponse<CountResponse, C>>;
  msearch<T = unknown, R extends RequestNDBody = Record<string, any>[], C = unknown>(
    params: RequestParams.Msearch<R>,
    options?: TransportRequestOptions,
  ): Promise<ApiResponse<MsearchResponse<T>, C>>;
  get<T = unknown, C = unknown>(
    params: RequestParams.Get,
    options?: TransportRequestOptions,
  ): Promise<ApiResponse<GetResponse<T>, C>>;
  exists<C = unknown>(
    params: RequestParams.Exists,
    options?: TransportRequestOptions,
  ): Promise<ApiResponse<boolean, C>>;
  update<R extends RequestBody = Record<string, any>, C = unknown>(
    params: RequestParams.Update<R>,
    options?: TransportRequestOptions,
  ): Promise<ApiResponse<void, C>>;
  updateByQuery<R extends RequestBody = Record<string, any>, C = unknown>(
    params: RequestParams.UpdateByQuery<R>,
    options?: TransportRequestOptions,
  ): Promise<ApiResponse<UpdateDocumentByQueryResponse, C>>;
  delete<C = unknown>(params: RequestParams.Delete, options?: TransportRequestOptions): Promise<ApiResponse<void, C>>;
  bulk<R extends RequestNDBody = Record<string, any>[], C = unknown>(
    params: RequestParams.Bulk<R>,
    options?: TransportRequestOptions,
  ): Promise<ApiResponse<IBulkResponse, C>>;
  bulkHelper<T = unknown>(options: BulkHelperOptions<T>): Promise<BulkStats>;
}
