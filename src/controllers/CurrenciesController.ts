import { inject } from 'inversify';

import { IDENTIFIERS } from '@configurations/constants';
import { APIGatewayProxyEventWithBody, APIGatewayProxyResultWithStatus } from '@configurations/types';
import { action, controller } from '@core/decorators';
import { GetCurrency } from '@interfaces/ICurrenciesController';
import { ICurrenciesService } from '@interfaces/ICurrenciesService';
import { ResponseHelper } from '@helpers/ResponseHelper';
import { ICurrency } from '@models/ICurrencies';

@controller()
export class CurrenciesController {
  constructor(
    @inject(IDENTIFIERS.CURRENCIES_SERVICE)
    public currenciesService: ICurrenciesService,
  ) {}

  @action('getAllCurrencies')
  async getCurrencies() {
    const currencies = await this.currenciesService.getCurrencies();

    return ResponseHelper.handle(currencies);
  }

  @action('getCurrency')
  async getCurrency(
    event: APIGatewayProxyEventWithBody<GetCurrency>,
  ): Promise<APIGatewayProxyResultWithStatus<ICurrency>> {
    const currency = await this.currenciesService.getCurrency(event.body.code);

    return ResponseHelper.handle(currency);
  }
}
